/*
Name : Kunal Parakh
USC iD: 8437861101
File: doctor.cpp
Description: Patient will initiate communication with the assigned doctor and will be served based on the insurance value.
*/

#include "header.h"

void *doctor_proc(void *);

class Insurance_Details{			// Class to store insurance details
	public:
		string insuranceType;
		string insuranceCost;

		Insurance_Details(string type,string cost){
			insuranceType=type;
			insuranceCost=cost;
		}	
};

list<Insurance_Details > Doctor1_insuranceDetails;		// List to store insurance details
list<Insurance_Details > Doctor2_insuranceDetails;

int main(){

	string insuranceType,insuranceCost,line;
	char Doctor1_port[MAXDATASIZE],Doctor2_port[MAXDATASIZE];
	
	pthread_t *Doctor_thread[2];

	Doctor_thread[0]=(pthread_t *)malloc(sizeof(pthread_t));
	Doctor_thread[1]=(pthread_t *)malloc(sizeof(pthread_t));		//Thread memory allocation
											
	ifstream f1 ("doc1.txt"); 														
	ifstream f2 ("doc2.txt");		

/*Start of code to read doctor files*/													
	if(f1.is_open())																	
	{
		while(getline(f1,line))
		{
			insuranceType=line.substr(0,10);
			insuranceCost=line.substr(11);
			Doctor1_insuranceDetails.push_back(Insurance_Details(insuranceType,insuranceCost));			
		}
		f1.close();
	}
	else
	{
		printf("File doc1.txt not found\n" );
	}
	if(f2.is_open())																	
	{
		while(getline(f2,line))
		{
			insuranceType=line.substr(0,10);
			insuranceCost=line.substr(11);
			Doctor2_insuranceDetails.push_back(Insurance_Details(insuranceType,insuranceCost));			
		}
		f2.close();
	}
	else
	{
		printf("File doc2.txt not found\n" );
	}

/*End of code to read doctor files*/

	strcpy(Doctor1_port,DOC1_PORT);
	strcpy(Doctor2_port,DOC2_PORT);

	if(pthread_create(Doctor_thread[0], NULL, doctor_proc, Doctor1_port))
			perror("cannot create doctor thread.\n");						// Thread create
	if(pthread_create(Doctor_thread[1], NULL, doctor_proc, Doctor2_port))
		perror("cannot create doctor thread.\n");
	
	if(*Doctor_thread[0])
		pthread_join(*Doctor_thread[0],NULL);								// Thread join
	if(*Doctor_thread[0])
		pthread_join(*Doctor_thread[1],NULL);

	return 0;
}

// Threaded procedure for doctor
void *doctor_proc(void *doc_port)
{
	char Doctor_port[MAXDATASIZE],doc[MAXDATASIZE];

	strcpy(Doctor_port,(char *) doc_port);
	if(!strcmp(Doctor_port,DOC1_PORT))
		strcpy(doc,"Doctor 1");					// Decide which doctor name to assign to the port
	else if(!strcmp(Doctor_port,DOC2_PORT))
		strcpy(doc,"Doctor 2");
	
	int fd, numbytes, rv, port = -1,count=0;
    char buf[MAXDATASIZE];
    struct addrinfo hints, *servinfo, *p;
    struct sockaddr_in their_addr; 			
    struct sockaddr_in self;
    socklen_t addr_len;
    socklen_t len = sizeof (self);
	string cost;
	char *Doctor_ip;						

    memset(&hints, 0, sizeof hints);
    memset(&servinfo, 0, sizeof servinfo);		// Beeg's code
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;

    if ((rv = getaddrinfo(HOST, Doctor_port, &hints, &servinfo)) != 0) {
        fprintf(stderr, "Phase 3 %s: Failed to getaddrinfo() for destination %s. Exiting\n",doc, gai_strerror(rv));	// Beeg's code
        exit(1);
    }

    // loop through all the results and make a socket
    for (p = servinfo; p != NULL; p = p->ai_next) {
        if ((fd = socket(p->ai_family, p->ai_socktype,p->ai_protocol)) == -1) {		// Beeg's code
            perror("Phase 3: Couuld not create socket.\n");
            continue;
        }
        if (bind(fd, p->ai_addr, p->ai_addrlen) == -1) {
            close(fd);																		// Beeg's code
            perror("Phase 3: Failed to bind receiving port.\n");
            continue;
        }
        break;
    }

    if (p == NULL) {
        fprintf(stderr, "Phase 3 %s: Could not bind socket. Exiting.\n",doc);
        exit(2);
    }

    len = sizeof (self);

    if (getsockname(fd, (struct sockaddr *) &self, &len) == -1){
        perror("Phase 3: getsockname() failed.\n");						// Beeg's code
    }
    port = -1;
    port = ntohs(self.sin_port);
    if (port == -1)
        perror("Phase 3: ntohs() failed.\n");
    Doctor_ip = inet_ntoa(self.sin_addr);

    printf("Phase 3 %s has static UDP port number %d and IP address %s.\n", doc, port, Doctor_ip);

    while(count<2){
    	
    	addr_len = sizeof their_addr;
    
		if ((numbytes = recvfrom(fd, buf, MAXDATASIZE-1, 0, (struct sockaddr *) &their_addr, &addr_len)) == -1) {
			perror("Phase 3: recvfrom() failed. Exiting.\n");											// Beeg's code
			exit(1);
		} 
		else {	
			
			int Patient_port = -1;
		    Patient_port = ntohs(their_addr.sin_port);
		    if (Patient_port == -1)
		        perror("Phase 3: error extracting PORT using ntohs().\n");
		    buf[numbytes] = '\0';
		    
		    if( (strcmp(buf,"insurance1")< 0 || strcmp(buf,"insurance1")>0) && (strcmp(buf,"insurance2")< 0 || strcmp(buf,"insurance2")>0) )
				continue;

		    printf("%s receives a request from the patient with port number %d and the insurance plan %s.\n",doc,Patient_port,buf);
		    count++;		

		    if(!strcmp(Doctor_port,DOC1_PORT) )
		    {
		    	for(list<Insurance_Details>:: iterator it=Doctor1_insuranceDetails.begin();it!=Doctor1_insuranceDetails.end();++it){
		    		if(!strcmp(buf, (*it).insuranceType.c_str()  ) )
		    			cost=(*it).insuranceCost;					
		    	}
		    }
		    else if(!strcmp(Doctor_port,DOC2_PORT))
		    {
		    	for(list<Insurance_Details>:: iterator it=Doctor2_insuranceDetails.begin();it!=Doctor2_insuranceDetails.end();++it){
		    		if(!strcmp(buf, (*it).insuranceType.c_str()  ))
		    			cost=(*it).insuranceCost;							
		    	}
		    }
		    char estimate[MAXDATASIZE];
		    strcpy(estimate,cost.c_str());
		 		    
		    if( (numbytes=sendto(fd, estimate, strlen(estimate), 0,(struct sockaddr *) & their_addr, addr_len)) == -1){
		    	perror("Phase 3: Patient 1 sendto failed. Exiting. \n");
	        	exit(1);															// Beeg's code
		    }  		
		   
		}
	}
    close(fd);
    return NULL;
}
