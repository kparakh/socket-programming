/*
Name : Kunal Parakh
USC iD: 8437861101
File: patient2.cpp
Description: Patient 2 requests Health Server Center for available slots and communicates with the allocated doctor.
*/

#include "header.h" //include all the header files and macros


void UDPcreate(char *);
// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
	if (sa->sa_family == AF_INET) {
		return &(((struct sockaddr_in*)sa)->sin_addr);
}
	return &(((struct sockaddr_in6*)sa)->sin6_addr);
}
int main ()
{

/*start of code to open patient2.txt and reading contents */
	ifstream fp2("patient2.txt"); 
	string username,password,line;
	
	if(fp2.is_open())
	{
		line.clear();
		while(getline(fp2,line))
		{
			username=line.substr(0,8); //split the username and password , copy them in respective strings
			password=line.substr(9);
		}
		fp2.close();
	}
	else
	{
		printf("\nFile patient2.txt not found");
	}
/*end of code to open patient2.txt and reading contents*/	
	
	int sockfd, numbytes;
	char buf[MAXDATASIZE],buffer_apt[MAXDATASIZE];
	struct addrinfo hints, *servinfo, *p; 	//socket variables and structures
	struct sockaddr_in addr_inet;
	int rv;
	//char s[INET_ADDRSTRLEN];

	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	if ((rv = getaddrinfo(HOST, PORT, &hints, &servinfo)) != 0) {			// Beeg's code
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));
		return 1;
	}	
	//look for the first socket connection encountered
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype,p->ai_protocol)) == -1) {
			perror("Patient2: socket\n");									// Beeg's code
			continue;
		}
		if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			perror("Patient2: connect\n");						// Beeg's code
			continue;
		}
		break;
	}
	if (p == NULL) {
		fprintf(stderr,"Patient2: failed to connect\n");
		return 2;
	}

	socklen_t len=sizeof(struct sockaddr);	
	int getsock_check=getsockname(sockfd, (struct sockaddr *) &addr_inet, &len);

	if(getsock_check==-1){
  		perror("getsockname");
  		exit(1);
	} 
    printf("Phase 1: Patient 2 has TCP port number %u and IP address %s.\n",ntohs(addr_inet.sin_port),inet_ntoa(addr_inet.sin_addr));
	
	freeaddrinfo(servinfo); 
	
	string message="authenticate "+username+" "+password;

		if (send(sockfd,message.c_str(),message.length()+1,0)==-1)
		{
			perror("send");				// Beeg's code
			exit(0);
		}else
		{
			printf("Phase 1: Authentication request from Patient 2 with username %s and password %s has been sent to Health Center Server.\n",username.c_str(),password.c_str());
		}

		if ((numbytes = recv(sockfd, buf, MAXDATASIZE-1, 0)) == -1) {
			perror("recv");				// Beeg's code
			exit(1);
		}
		buf[numbytes] = '\0';
			printf("Phase 1: Patient 2 authentication result: %s\n", buf);
			printf("Phase 1: End of Phase 1 for Patient1.\n");
		if(strcmp(buf,"failure")==0)			//failed authentication
			exit(1);
		else if(strcmp(buf,"success")==0)			//authentication successful
		{	
			string msg_buffer = "available";
			
			if (send(sockfd,msg_buffer.c_str(),(msg_buffer).length()+1,0)==-1)
				perror("send");											// Beeg's code
			if ((numbytes = recv(sockfd, buffer_apt, MAXDATASIZE-1, 0)) == -1) 
			{
				perror("recv");
				exit(1);	
			}
			else{
					unsigned int i=0;
					string ch;
					int valid =0;
					buffer_apt[numbytes] = '\0';
					string index_list,avail;
					printf("Phase 2: The following appointments are available for Patient 2:\n%s\n",buffer_apt);
				while(valid!=1)
				{ 
					printf("Please enter the preferred appointment index and press enter:");
					cin>>ch;

					index_list = buffer_apt;
						for(i=0;i<(index_list).length();i++)
						{
							avail += index_list[i];
							i = i+10;
						}
						if (avail.find(ch) != string::npos) 
						{
							valid = 1;
							ch = "selection " + ch;
						}
				}	
					if(valid == 1)
					{
						if (send(sockfd,ch.c_str(),ch.length()+1,0)==-1)
						perror("send");
					}
					char doc_info[MAXDATASIZE];
					if ((numbytes = recv(sockfd, doc_info, MAXDATASIZE-1, 0)) == -1) 
					{
							perror("recv");			// Beeg's code
							exit(1);
					}

					else{
						doc_info[numbytes] = '\0';
						if(!strcmp(doc_info,"notavailable"))
						{
							printf("Phase 2: The requested appointment from Patient 2 is not available. Exiting...\n");
							exit(1);
						}
						else
						{
							char *temp_doc = NULL,*doc_port=NULL;
							temp_doc = strtok(doc_info," ");
							strcpy(temp_doc,"");
							doc_port = strtok(NULL, " ");
							printf("Phase 2: The requested appointment is available and reserved to Patient 2. The assigned doctor port number is %s.\n",doc_port);
							UDPcreate(doc_port);
						}
						
					}
			}
		}
	close(sockfd);	
	return 0;
}

void UDPcreate(char *dp)
{
	
	int fd, numbytes;
    char buf[MAXDATASIZE];
    struct addrinfo hints, *servinfo, *p;
    struct sockaddr_in their_addr; 			
    int rv;
    struct sockaddr_in myAddr;
    socklen_t addr_len;
	char portDoc[MAXDATASIZE];
    
    addr_len = sizeof their_addr;
    
    if(dp[1] == '1')
    strcpy(portDoc,DOC1_PORT);
    
    else if(dp[1] == '2')
    strcpy(portDoc,DOC2_PORT);

	memset(&hints, 0, sizeof hints);
	memset(&servinfo, 0, sizeof servinfo);		// Beeg's variabes
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_DGRAM;

    if ((rv = getaddrinfo(HOST, portDoc, &hints, &servinfo)) != 0) {		
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));		// Beeg's code
        exit(1);
    }

    for(p = servinfo; p != NULL; p = p->ai_next) {
        if ((fd = socket(p->ai_family, p->ai_socktype,p->ai_protocol)) == -1) {
            perror("Phase 3: Couuld not create socket.\n");			// Beeg's code
            continue;
        }
        break;
    }

    if (p == NULL) {
        perror("Phase 3: Couuld not bind socket.\n");
        exit(2);
    }
    
/*start of code to open patient1insurance.txt and reading contents */
	ifstream fp1("patient1insurance.txt"); 
	string line,_typeInsurance;
	char typeInsurance[MAXDATASIZE];
	if(fp1.is_open())
	{
		while(getline(fp1,line))
		{
			_typeInsurance=line;
		}
		fp1.close();
	}
	else
	{
		printf("\nFile patient1insurance.txt not found");
	}
/*end of code to open patient1insurance.txt and reading contents*/	

	strcpy( typeInsurance,_typeInsurance.c_str());

    if (( numbytes = sendto(fd, typeInsurance, strlen(typeInsurance), 0,p->ai_addr,p->ai_addrlen) ) == -1) {
        perror("Phase 3: Patient 2 sendto failed. Exiting. \n");
        exit(1);
    }

    else{
    	
	    socklen_t len = sizeof (myAddr);

	    if (getsockname(fd, (struct sockaddr *) &myAddr, &len) == -1)
	        perror("Phase 3: getsockname() failed.\n");		// Beeg's code

	    int port = -1;
	    port = ntohs(myAddr.sin_port);
	   
	    if (port == -1)
	        perror("Phase 3: ntohs() failed.\n");

	    struct hostent *hp;
	    char *ipstr=NULL;
	    hp = (struct hostent *)gethostbyname(HOST);
	    myAddr.sin_addr = *((struct in_addr *)hp->h_addr);
	    ipstr = inet_ntoa(myAddr.sin_addr);

	    printf("Phase 3: Patient 2 has a dynamic UDP port number %d and IP address %s.\n",port, ipstr);
    }
    if ((numbytes = recvfrom(fd, buf, MAXDATASIZE - 1, 0,(struct sockaddr *)&their_addr, &addr_len)) == -1) {
        perror("Phase 3: recvfrom() failed. Exiting.\n");
        exit(1);
	}
	else
	{
		buf[numbytes]='\0';
		char *IP_doc = NULL;
		IP_doc = inet_ntoa(their_addr.sin_addr);
		char docName[MAXDATASIZE];
		if(strcmp(DOC1_PORT,portDoc)==0) strcpy(docName,"Doctor 1");
		else if(strcmp(DOC2_PORT,portDoc)==0) strcpy(docName,"Doctor 2");
		printf("Phase 3: The cost estimation request from Patient 2 with insurance plan %s has been sent to doctor with port number %s and IP address %s.\n",typeInsurance,portDoc,IP_doc );
		printf("Phase 3: Patient 2 receives %s$ estimation from doctor with port number %s and name %s.\n",buf,portDoc,docName);
		printf("Phase 3: End of Phase 3 for Patient 2.\n");
	}
	close(fd);
}