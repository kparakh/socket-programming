#include <iostream>
#include <list>
#include <string>
#include <fstream>
#include <stdlib.h>
#include <stdio.h>
#include <map>
 
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/wait.h>
#include <signal.h>
#include <pthread.h>

#define PORT "21101"		// TCP port for health center server
#define DOC1_PORT "41101"	// UDP Port dor doctor1
#define DOC2_PORT "42101"	// UDP Port for doctor2
#define BACKLOG 10 		
#define MAXDATASIZE 1024	// Max buffer size
#define HOST "nunki.usc.edu"// Host name
 using namespace std;