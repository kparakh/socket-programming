all: healthcenterserver doctor patient1 patient2
healthcenterserver: healthcenterserver.cpp 
	g++ -Wall -o healthcenterserver healthcenterserver.cpp -lresolv -lnsl -lpthread
doctor: doctor.cpp 
	g++ -Wall -o doctor doctor.cpp -lresolv -lnsl -lpthread
patient1: patient1.cpp 
	g++ -Wall -o patient1 patient1.cpp -lresolv -lnsl 
patient2: patient2.cpp 
	g++ -Wall -o patient2 patient2.cpp -lresolv -lnsl 
clean:
	rm -f healthcenterserver doctor patient1 patient2
