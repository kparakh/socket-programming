Name : Kunal Parakh
USC ID: 8437861101

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

Short Description -

In the projet, I have utilized features of both C and C++ as C++ is much easier when it comes to creating lists to store information and string manipulations.

This is a socket programming assignment with three phases. 
1. In Phase 1, I have created a TCP connectionn between patient and health center server in which the health center server will check for only the registered patients based on the credentials provided by the patient. Once the patient is been authorized, it moved ahead to Phase 2.

2. In Phase 2, the same connection continues and the patients would requuest the health center server for available time solts and the server would send the list accordingly. I have also made use of threading to create child and parent sockets so that the concurrency is served properly when two patients request at the same time.

3. In Phase 3, patient would make use of the allotted doctor port from health center to setup a UDP socket with the doctor. Here also I have made use of threading to create two doctor threads so that they can serve individually and terminate once the patient is been attended.

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

Files - 

1. healthcenterserver.cpp - The Halth Center Server supporting TCP, authenticating patients and allotting slots to them
2. doctor.cpp - A thread operated program creating doctors and letting them attend the patients
3. patient1.cpp - A program for patient1 communicating with health center server and doctor.
3. patient2.cpp - A program for patient2 communicating with health center server and doctor.
4. header.h - Includes all header files, Static ports and Host name.
5. Makefile - A simple makefile to compile all the four files together.
6. availabilities.txt - List of available time slots and doctor ports.
7. patient1.txt, patient2.txt - Login credentials for patients. 
8. doc1.txt, doc2.txt - Insurances accepted by doctors and cost associated.
9. patient1insurance.txt, patient2insurance.txt - Insurances taken by the patients.

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

Steps to run the program - 

1. Please put all the input files in the directory before running "make" command.
2. Run the "make clean" command which will delete all the output files if any. Then run "make" command to generate healthcenterserver.out, doctor.out, patient1.out, patient2.out.
3. Start the heath center server, then the run the doctor.
4. Run the patient executables and start the communication.
5. The project serves concurrent users so it can be tested.
6. Manually terminate the doctor program threads once the patients are served.

Note : Please test the multithreaded implementation for doctor and concurrency control for the patients.

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

Format of messages -

The format of messages is the same as specified in the project description document. No additional messages involved.

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

Idiosyncrasies - 

The project works fine in all the cases except sometimes it fails to bind as the port is busy. Please make sure to kill the proesses and free the ports before running "make" command.

-------------------------------------------------------------------------------------------------------------------------------------------------------------------

Reused Code - 

I have picked up sample templates from Beej's guide which mainly consist of standard socket functions and send-receive call syntax.

References -
1. http://www.cplusplus.com/  --- C++ lists and strings
2. https://computing.llnl.gov/tutorials/pthreads/   --- Thread tutorials
3. https://stackoverflow.com   ---  Minor errors and problems

All the reused code is marked and commented in the source files.

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
