/*
Name : Kunal Parakh
USC iD: 8437861101
File: healthcenterserver.cpp
Description: Health Center Server accepting requests from patients and allocating available slots to the patients.
*/

#include "header.h"  //include all the header files and macros
void *patientrequest(void *);	

class Availability_Details{	   //class to store contents of availabilities.txt
	public:
		string index;
		string day;		
		string appt_time;
		string doctor;
		string port;
		string status;
 	
 		Availability_Details(string i,string d,string time1, string doc, string p, string s){
			index=i;
			day=d;
			appt_time=time1;
			doctor=doc;
			port=p;
			status=s;
		}
		
};

list< Availability_Details > Availability_List;   //creating a list of availabilities' contents
map<string, string > user;	
pthread_mutex_t mutex;		
		
int main ()
{
	pthread_t *patient_thread[2];
	patient_thread[0]=patient_thread[1]=NULL;   //Thread variables
	int patient_fd[2];

	int sockfd,rv,port,i=0;  					// Listen on sockfd
	int patientcount=0; 			 			// Keep track of number of patients.
	struct addrinfo hints, *servinfo, *p;
	struct sockaddr_in server;
	socklen_t length = sizeof(server);
	struct sockaddr_storage their_addr; 		// Connector's address information
	socklen_t sin_size;
	char *ipstr = NULL;

	if(pthread_mutex_init(&mutex,NULL))
	{
		printf("Unable to intitalize mutex\n");  // Thread initialization
		exit(1);
	}

	string avail_day,avail_time,avail_doc,line,username,password,avail_index,avail_port;
	ifstream availabilities ("availabilities.txt"); 	
	ifstream users("users.txt");						
	
	if(availabilities.is_open())
	{
		while(getline(availabilities,line))
		{
			avail_index=line.substr(0,1);
			avail_day=line.substr(2,3);
			avail_time=line.substr(6,4);
			avail_doc=line.substr(11,4);
			avail_port=line.substr(16);
			Availability_List.push_back(Availability_Details(avail_index,avail_day,avail_time,avail_doc,avail_port,"available"));			
		}
		availabilities.close();
	}
	else
	{
		fprintf(stderr, "File availabilities.txt not found\n" );
	}

	if(users.is_open())
	{
		line.clear();
		while(getline(users,line))
		{
			username=line.substr(0,8);
			password=line.substr(9);
			user[username]=password;
		}
		users.close();
	}
	else
	{
		fprintf(stderr, "File users.txt not found\n" );
	}
	
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;			// Beeg's variable list
	hints.ai_flags = AI_PASSIVE; 
	if ((rv = getaddrinfo(HOST, PORT, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(rv));			// Beeg's code
		return 1;
	}
	
	//Create socket
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype,p->ai_protocol)) == -1) {		// Beeg's code
			perror("server: socket");
			continue;
		}
	//Bind Socket
		if (bind(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);								// Beeg's code
			perror("server: bind");
			continue;
		}
		break;
	}
	if (p == NULL) {
		fprintf(stderr, "server: failed to bind\n");
		return 2;
	}
	
	if(getsockname(sockfd,(struct sockaddr *)&server, &length) == -1)
		perror("getsockname");
		ipstr = inet_ntoa(server.sin_addr);
		port = ntohs(server.sin_port);

	   printf("Phase 1: The Health Center Server has port number %d and IP address %s.\n", port,ipstr);
	
	if (listen(sockfd, BACKLOG) == -1) {
		perror("listen");
		exit(1);
	}
	
	while(i<2)
	{ 
		if(patientcount>2)
		{
			break;				// More than two patients requesting connection to health center
		}

		sin_size = sizeof their_addr;
		patient_fd[i] = accept(sockfd, (struct sockaddr *)&their_addr, &sin_size);
		if (patient_fd[i] == -1) {
			perror("accept");
			continue;
		}

		patientcount++;  

		patient_thread[i] = (pthread_t *)malloc(sizeof(pthread_t));		// Thread memory allocation

		if(pthread_create(patient_thread[i],NULL,patientrequest,&patient_fd[i]))
			perror("\nUnable to create Patient.\n");					// Thread creation
		i++;	
	
	}

	if(*patient_thread[0])
		pthread_join(*patient_thread[0],NULL);
																		// Thread join
	if(*patient_thread[1])
		pthread_join(*patient_thread[1],NULL);

	close(sockfd); // Close parent socket. Terminate TCP connection
	return 0;
}

// Patient Request Thread Process
void *patientrequest(void *arg)
{
	char *client_addr = NULL;
	int client_port = -1;
	char buf[MAXDATASIZE],buffer_1[MAXDATASIZE];
	string buf_send;
	int numbytes;
	char *uname,*pwd,*temp;
	int *fd;
	fd = (int *)arg;
	struct sockaddr_in client;
	socklen_t length_client = sizeof(client);


	int getpeer = getpeername(*fd,(struct sockaddr *)&client,&length_client);
	if (getpeer == -1) {
		perror("peername");
		exit(1);
	}												// Fetch patient information
	client_port = ntohs(client.sin_port);
	if (client_port == -1) {
		perror("clientport");
	}
	client_addr = inet_ntoa(client.sin_addr);
	
	if ((numbytes = recv(*fd, buf, MAXDATASIZE-1, 0)) == -1) {  
			perror("recv");
		}																		// Beeg's code
		buf[numbytes] = '\0';
		
	temp=strtok(buf," ");
	if(!strcmp(temp,"authenticate")) // authenticate keyword
	{   
		uname=strtok(NULL," ");
		pwd=strtok(NULL," ");		// Tokenize to check for match
		printf("Phase 1: The Health Center Server has received request from a patient with username %s and password %s.\n",uname,pwd);
		
		if(strcmp( pwd, (user.find(uname)->second).c_str() ) )
			buf_send="failure";		// No match
		else
			buf_send="success";		// Match
		
		if (send(*fd, buf_send.c_str(), buf_send.length()+1, 0) == -1)
			perror("send");
		else{
			printf("Phase 1: The Health Center Server sends the response %s to patient with username %s.\n",buf_send.c_str(),uname);
		}
	
		if ((numbytes = recv(*fd, buffer_1, MAXDATASIZE-1, 0)) == -1) {  
				perror("recv");
		}
		buffer_1[numbytes] = '\0';

		temp=strtok(buffer_1," ");
		if(!strcmp(temp,"available")) // available keyword
		{
			string buff_available;
			char choice[MAXDATASIZE];
			printf("Phase 2: The Health Center Server, receives a request for available slots from patients with port number %d and IP address %s.\n",client_port,client_addr);
			for (list<Availability_Details>::iterator it = Availability_List.begin(); it != Availability_List.end(); it++){
			
				if((*it).status.compare("available")==0)
				{
					buff_available += (*it).index+" "+ (*it).day+" "+(*it).appt_time+"\n";
				}
			}
			if (send(*fd, buff_available.c_str(), buff_available.length()+1, 0) == -1)
			{
				perror("send");													// Beeg's code
			}
			else
			{
				printf("Phase 2: The Health Center Server sends available slots to patient with username %s.\n",uname);
			}

			if ((numbytes = recv((*fd), choice, MAXDATASIZE-1, 0)) == -1) {  
				perror("recv");													// Beeg's code
			}
			else
			{
				choice[numbytes] = '\0';
				int reserved = 0;
				char * temp_choice=strtok(choice," ");
				if(!strcmp(temp_choice,"selection"))  // selection keyword
				{
					char *ch = strtok(NULL," ");
					printf("Phase 2: The Health Center Server recieves a request for appointment %s from patient with port number %d and username %s.\n",ch,client_port,uname);
					
					pthread_mutex_lock(&mutex);   // Thread lock 

					for (list<Availability_Details>::iterator it = Availability_List.begin(); it != Availability_List.end(); it++)
					{
						if((*it).status.compare("available")==0 && (*it).index.compare(ch)==0)
						{
							reserved = 1;
							(*it).status = "reserved";
						}
						if(reserved)
						{
							
							string docinfo = (*it).doctor + " " + (*it).port;
							if (send(*fd, docinfo.c_str(), docinfo.length()+1, 0) == -1)
								{
									perror("send");
								}
								else
									printf("Phase 2: The Health Center Server confirms the following appointment %s to patient with username %s.\n",ch,uname);
								break;
						}
					}
					pthread_mutex_unlock(&mutex);	// Thread unlock 
					if(reserved != 1)
					{
						string reject = "notavailable";
						if (send(*fd, reject.c_str(), reject.length()+1, 0) == -1)
							{
								perror("send");
							}
							else
							{
								printf("Phase 2: The Health Center Server rejects the following appointment %s to patient with username %s.\n",ch,uname);							
							}
					}

				}
			}	
		}	
	}
	close(*fd);   // Terminate socket thread
	return NULL;	
}
